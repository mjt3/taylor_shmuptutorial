﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour 
{

	public GameObject hazard;
	public GameObject explosion;

	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;

	public GUIText lifeText;
	public int life;

	public GUIText bombText;
	public int bomb;

	private bool gameOver;
	private bool restart;
	private int score;



	void Start ()
	{
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		life = 3;
		bomb = 2;
		UpdateScore ();
		UpdateLife ();
		UpdateBomb ();
		StartCoroutine (SpawnWaves ());
	}

	void Update ()
	{
		if (restart) 
		{
			if (Input.GetKeyDown (KeyCode.R)) 
			{
				Application.LoadLevel (Application.loadedLevel);
			}
		}

		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			if (bomb > 0) 
			{
				DestroyAllAsteroids ();
				newBombValue ();
			}
		}
	}

	IEnumerator SpawnWaves ()
	{
		yield return new WaitForSeconds (startWait);
		while (true)
		{
			for (int i = 0; i < hazardCount; i++) 
			{
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValues.x, spawnValues.x), spawnValues.y, spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
				hazardCount += 1;
			}
			yield return new WaitForSeconds (waveWait);

			if (gameOver) 
			{
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}

	public void AddScore (int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore ();
	}

	public void LoseLife ()
	{
		life -= 1;
		UpdateLife ();
	}

	public void newBombValue ()
	{
		bomb -= 1;
		UpdateBomb ();
	}

	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
	}

	void UpdateLife()
	{
		lifeText.text = "Life: " + life;
	}

	void UpdateBomb ()
	{
		bombText.text = "Bombs: " + bomb;
	}
		

	void DestroyAllAsteroids()
	{
		var asteroidObjects = GameObject.FindGameObjectsWithTag ("Asteroid");

		for (var i = 0; i < asteroidObjects.Length; i ++) 
		{
			Destroy (asteroidObjects [i]);
		}
	}

	public void GameOver ()
	{
		gameOverText.text = "Game Over!";
		gameOver = true;
	}
}